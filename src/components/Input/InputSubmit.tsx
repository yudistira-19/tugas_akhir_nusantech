import React from 'react'

interface ISubmit {
  value: string
  className: string
  icon?: React.ReactNode
  onClick?: () => void
}

function InputSubmit(props: ISubmit) {
  const { value, className, icon, onClick } = props

  return (
    <button
      className={`${className} flex items-center`}
      type="submit"
      onClick={onClick} // Tambahkan onClick ke button
    >
      <div className="m-auto flex justify-items-center items-center gap-4 text-[16px]">
        {value}
        <span className="icon mr-2 mb-2">{icon}</span>
      </div>
    </button>
  )
}

export default InputSubmit
